/**
 * 
 */
package com.cencor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author gusvmx
 *
 */
@SpringBootApplication
@EnableSwagger2
public class MyApplication {

    /***/
    private static final Logger LOGGER = LoggerFactory.getLogger(MyApplication.class);
    
    /**
     * @param args
     */
    public static void main(final String[] args) {
        SpringApplication.run(MyApplication.class);
        LOGGER.info("Aplicacion inicializada");
    }

}
