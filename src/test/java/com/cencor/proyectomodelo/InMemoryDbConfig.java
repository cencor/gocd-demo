/**
 * 
 */
package com.cencor.proyectomodelo;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import liquibase.integration.spring.SpringLiquibase;

/**
 * @author gusvmx
 *
 */
@Configuration
@EnableJpaRepositories(basePackages =
        {
                "com.cencor.proyectomodelo" //TODO Cambiar al paquete correcto
        })
@EnableTransactionManagement
@ComponentScan({"com.cencor.proyectomodelo"}) //TODO Cambiar al paquete correcto
public class InMemoryDbConfig {
    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.HSQL)
                .addScripts(
                        "classpath:db/hsql-sql-server-config.sql",
                        "classpath:db/scripts/schema.sql", // TODO Eliminar si todo lo de BD se maneja por Liquibase
                        "classpath:db/scripts/data.sql" // TODO Eliminar si todo lo de BD se maneja por Liquibase
                )
                .build();
    }

    @Bean
    public SpringLiquibase liquibase() {
        SpringLiquibase lb = new SpringLiquibase();
        lb.setDataSource(dataSource());
        lb.setChangeLog("classpath:db/changelogMaster.xml");
        lb.setContexts("TEST");

        return lb;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();

        LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean 
            = new LocalContainerEntityManagerFactoryBean();
        localContainerEntityManagerFactoryBean.setJpaVendorAdapter(hibernateJpaVendorAdapter);
        //TODO Cambiar al paquete correcto
        localContainerEntityManagerFactoryBean.setPackagesToScan("com.cencor.proyectomodelo");
        localContainerEntityManagerFactoryBean.setDataSource(dataSource());
        return localContainerEntityManagerFactoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(final EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(entityManagerFactory);
        return jpaTransactionManager;
    }
}

