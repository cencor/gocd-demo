/**
 * 
 */
package com.cencor.proyectomodelo.sample.deleteme.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cencor.proyectomodelo.sample.deleteme.entity.Person;

/**
 * @author gusvmx
 *
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {

}
