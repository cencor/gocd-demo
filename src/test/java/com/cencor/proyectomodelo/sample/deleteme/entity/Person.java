/**
 * 
 */
package com.cencor.proyectomodelo.sample.deleteme.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author gusvmx
 *
 */
@Entity
@Table(name = "Person")
public class Person {

    /***/
    @Id
    private Integer id;
    
    /***/
    @Column(name = "nombre")
    private String name;

    /**
     * @return the id
     */
    public final Integer getId() {
        return id;
    }

    /**
     * @return the name
     */
    public final String getName() {
        return name;
    }
    
    /**
     * Asigna el nombre de la persona.
     * @param firstName El nombre
     * @param lastName El apellido.
     */
    public final void setName(final String firstName, final String lastName) {
        StringBuilder fullName = new StringBuilder();
        fullName.append(firstName);
        fullName.append(" ");
        fullName.append(lastName);
        
        this.name = fullName.toString();
    }

}
