package com.cencor.proyectomodelo.sample.deleteme.controller.rest;

import java.util.Optional;

import com.cencor.proyectomodelo.sample.deleteme.controller.PersonController;
import com.cencor.proyectomodelo.sample.deleteme.entity.Person;
import com.cencor.proyectomodelo.sample.deleteme.repository.PersonRepository;

import mockit.Expectations;
import mockit.Mocked;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Disabled
@SpringBootTest
public class PersonRestControllerTest {

    @Mocked
    private PersonRepository personRepository;

    @DisplayName("This is a test for find person!")
    @Test
    public final void findPerson() {
        PersonController controller = new PersonRestController(personRepository);
        Person gus = new Person();
        gus.setName("gus", "vargas");
        Optional<Person> mockedResult = Optional.of(gus);
        new Expectations() {{
            personRepository.findById((Integer) any);
            result = mockedResult;
        }};
        
        String name = controller.findById(1);
        
        assertEquals("gus vargas", name);
    }

    @DisplayName("This is a test with exception")
    @Test
    public final void personNotFound() {
        PersonController controller = new PersonRestController(personRepository);
        Optional<Person> mockedResult = Optional.empty();
        new Expectations() {{
            personRepository.findById((Integer) any);
            result = mockedResult;
        }};

        Exception exception = assertThrows(
                PersonNotFoundException.class, () -> controller.findById(1));

        assertEquals("person.not.found", exception.getMessage());
    }
}

