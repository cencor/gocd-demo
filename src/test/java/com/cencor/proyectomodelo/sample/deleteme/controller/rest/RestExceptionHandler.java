package com.cencor.proyectomodelo.sample.deleteme.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Locale;

/**
 * @author CAAC
 */
@ControllerAdvice
public class RestExceptionHandler {

    /**
     * Message resources.
     */
    private final MessageSource messageSource;

    /**
     * Constructor.
     * @param messageSource messageSource
     */
    @Autowired
    public RestExceptionHandler(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * Catch {@link PersonNotFoundException}.
     * @param ex exception
     * @param locale locale
     * @return ResponseEntity
     */
    @ExceptionHandler(PersonNotFoundException.class)
    public ResponseEntity<RestMessage> handleIllegalArgument(final PersonNotFoundException ex, final Locale locale) {
        String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
        return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);
    }

}
