/**
 * 
 */
package com.cencor.proyectomodelo.sample.deleteme.controller.rest;

/**
 * @author gusvmx
 *
 */
public class PersonNotFoundException extends RuntimeException {


    /***/
    private static final long serialVersionUID = 6141398580235993684L;

    /**
     * Message to exception.
     */
    private String message;

    /**
     * Arguments to replaced in message exception.
     */
    private Object[] args;

    /**
     * @param message Mensaje de error.
     * @param args argumentos
     */
    public PersonNotFoundException(final String message, final Object[]args) {
        this.args = args;
        this.message = message;
    }

    /**
     * Get Message.
     * @return message
     */
    @Override
    public String getMessage() {
        return message;
    }

    /**
     * Get args.
     * @return args
     */
    public Object[] getArgs() {
        return args;
    }
}
