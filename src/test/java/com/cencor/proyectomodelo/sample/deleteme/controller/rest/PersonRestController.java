/**
 * 
 */
package com.cencor.proyectomodelo.sample.deleteme.controller.rest;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cencor.proyectomodelo.sample.deleteme.controller.PersonController;
import com.cencor.proyectomodelo.sample.deleteme.entity.Person;
import com.cencor.proyectomodelo.sample.deleteme.repository.PersonRepository;

/**
 * @author gusvmx
 *
 */
@RestController
@RequestMapping("person")
public class PersonRestController implements PersonController {

    /***/
    private final PersonRepository personRepository;
    
    /**
     * @param personRepository El repositorio de personas.
     */
    public PersonRestController(@Autowired final PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    @GetMapping(path = "id/{id}")
    public final String findById(@PathVariable final int id) {
        Optional<Person> result = personRepository.findById(id);
        if (!result.isPresent()) {
            throw new PersonNotFoundException("person.not.found", new Object[]{id});
        }
        Person person = result.get();
        return person.getName();
    }

}
