/**
 * 
 */
package com.cencor.proyectomodelo.sample.deleteme.controller.rest;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author gusvmx
 *
 */
//@Disabled
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PersonRestControllerContractTest {

    @Autowired
    private TestRestTemplate restTemplate;
    
    @Test
    @DisplayName("Prueba de contrato para el endpoint")
    public final void findByIdContract() {
        String name = this.restTemplate.getForObject("/person/id/1", String.class);
        
        assertEquals("gus", name);
    }

    @Test
    @DisplayName("Prueba de contrato para el mensaje de error en inglés")
    public final void findByIdContractTestWithEnglishError() {
        HttpHeaders headersEnglish = new HttpHeaders();
        headersEnglish.put("Accept-Language", Arrays.asList("en-US"));

        HttpEntity request = new HttpEntity<>(headersEnglish);

        ResponseEntity<String> response = this.restTemplate.exchange("/person/id/100", HttpMethod.GET, request, String.class);

        assertTrue(response.getBody().contains("Person with id=100 do not exists!"));
    }

    @Test
    @DisplayName("Prueba de contrato para el mensaje de error en español")
    public final void findByIdContractTestWithSpanishError() {
        HttpHeaders headersEnglish = new HttpHeaders();
        headersEnglish.put("Accept-Language", Arrays.asList("es-MX"));

        HttpEntity request = new HttpEntity<>(headersEnglish);

        ResponseEntity<String> response = this.restTemplate.exchange("/person/id/100", HttpMethod.GET, request, String.class);

        assertTrue(response.getBody().contains("¡La persona con el id=100 no existe!"));
    }


}

