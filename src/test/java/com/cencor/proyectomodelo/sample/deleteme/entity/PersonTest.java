package com.cencor.proyectomodelo.sample.deleteme.entity;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class PersonTest {

    @Test
    public final void setFullName() {
        Person p = new Person();
        
        p.setName("Daniel", "Minutti");
        
        Assert.assertEquals("Daniel Minutti", p.getName());
    }
}

