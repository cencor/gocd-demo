/**
 * 
 */
package com.cencor.proyectomodelo.sample.deleteme.controller;

/**
 * @author gusvmx
 *
 */
public interface PersonController {

    /**
     * @param id El identificador de la persona.
     * @return El nombre de la persona.
     */
    String findById(int id);
}
