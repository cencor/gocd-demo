/**
 * 
 */
package com.cencor.proyectomodelo.sample.deleteme.repository;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import com.cencor.proyectomodelo.sample.deleteme.entity.Person;

/**
 * @author gusvmx
 *
 */
@Ignore
@ExtendWith(SpringExtension.class)
@DataJpaTest
public class PersonRepositoryTest {

    /***/
    @Autowired
    private PersonRepository personRepository;
    
    /***/
    @Test
    public final void getPerson() {
        Optional<Person> result = personRepository.findById(1);
        
        Assert.assertTrue(result.isPresent());
        Person gus = result.get();
        Assert.assertEquals(1, gus.getId().intValue());
        Assert.assertEquals("gus", gus.getName());
    }
}

