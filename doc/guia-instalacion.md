## Guía de instalación

La siguiente guía de instalación proporciona las instrucciones para instalar este cómponente en equipos que lo estarían ejecutando.

### Requisitos de instalación
Los siguientes requisitos deben ser cumplidos para una instalación exitosa.

#### BD

Indicar si requiere de alguna BD externa. Por ejemplo:
Este componente no requiere de una base de datos externa.

#### Requerimientos de software

* JVM - Amazon Correto 11.0.5

#### Requerimientos de hardware

##### Requerimientos mínimos

* Procesador: 1 CPU, 1 Core, 2.2Ghz
* Memoria RAM: 256Mb
* Almacenamiento: 200 Mb

##### Requerimientos recomendados

* Procesador: 1 CPU, 2 Core, 2.4Ghz
* Memoria RAM: 512Mb
* Almacenamiento: 400 Mb

#### Conectividad

Indicar todas las dependencias externas que tiene este componente. Por ejemplo:
* Tener acceso al servicio https://api.trkd.thomsonreuters.com/api/

### Construcción

#### Requisitos de software

* Gradle 5.3.1
* JVM Amazon Corretto 11.0.5
* Git

#### Obtención de código.

Para obtener el código es necesario descargarlo del repositorio Git de CENCOR. `$TAG_VERSION$` debe ser sustituido por la versión del componente a instalar.

```
git clone https://gitlab.com/cencor/nombre-repositorio.git`
cd nombre-repositorio
git checkout $TAG_VERSION$
```

#### Construcción

`gradle clean build`

La instrucción anterior genera el jar en la ruta `build/libs/nombre-repositorio-X.Y.Z.jar`. Donde `X.Y.Z` es la versión del componente a instalar.

#### Obtención de binario previamente construído por el servicio de integración continua.

Alternativamente se puede obtener el binario a instalar del repositorio de artefactos de CENCOR. La siguiente URL es una plantilla donde se debe sustituir `X.Y.Z` por la versión del componente a instalar.

https://artifact-repo.cencor.com/repository/maven-releases/com/cencor/nombre-repositorio/X.Y.Z/nombre-repositorio-X.Y.Z.jar

### Instalación

* Elegir una ruta de instalación. Por ejemplo `/opt/ruta/instalacion/nombre-repositorio`
* Elegir una ruta para los logs de la aplicación. Por ejemplo `/opt/ruta/instalacion/nombre-repositorio/logs`
* Copiar el jar de nombre `nombre-repositorio-X.Y.Z.jar` al directorio de instalación. El jar se puede obtener construyendo el componente o descargando directamente el Jar del repositorio de CENCOR.
* Copiar los shell ubicados en el repositorio en las rutas `etc/startup.sh` y `etc/shutdown.sh` al directorio de instalación

#### Configuración de variables de ambiente.

##### startup.sh

Asignar los valores a las siguientes variables definidas en el archivo `startup.sh`

* `JAVA_HOME` La ruta de instalación de la JVM de Amazon Correto
* `HOME_PATH` La ruta de instalación del componente

* `VERSION` La versión del componente a ejecutar.
* `LOG_LEVEL` Por default INFO pero puede cambiarse si fuese necesario.
* `LOG_FILE_PATH` La ruta donde se almacenarán los logs.

* `SERVICE_PORT` El puerto asignado a la aplicación. Más información en https://cencor.atlassian.net/wiki/spaces/CIYS/pages/838435109/Asignaci+n+de+puertos+TCP

##### shutdown.sh

Asignar los valores a las siguientes variables definidas en el archivo `shutdown.sh`

* `LOG_FILE_PATH` La ruta donde se almacenarán los logs

### Post-instalación

* Inicializar la aplicación mediante la ejecución del shell `startup.sh`
* Tras aproximadamente 20 segundos verificar que la aplicación haya inicializado correctamente mediante la busqueda de la siguiente palabra en el log de la aplicación: Application started. Por ejemplo `cat quote-service.log | grep "Application started"`
* Verificar estado de salud de la aplicación mediante el endpoint http://`ip`:`puerto`/actuator/health. Por ejemplo:

```
curl http://ip:puerto/actuator/health
{"status":"UP"}
```
* Alguna otra verificación adicional:

```
curl http://ip:puerto/endpoint-de-salud
```

### Troubleshooting