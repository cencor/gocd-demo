#!/bin/bash
VERSION=`head -1 version`

echo "Sustityendo variables en startup.sh"
sed -i 's#@JAVA_HOME@#'"$JAVA_HOME"'#g' etc/startup.sh
sed -i 's#@VERSION@#'"$VERSION"'#g' etc/startup.sh

echo "Desplegando bin y shells en $DEPLOY_DIR"
mkdir -p "$DEPLOY_DIR"
cp build/libs/grandioso-nombre-del-proyecto-"$VERSION".jar "$DEPLOY_DIR"
cp etc/startup.sh "$DEPLOY_DIR"
