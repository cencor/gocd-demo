#!/bin/bash
export PATH=$PATH:$JAVA_HOME/bin:$GRADLE_HOME/bin

version=$(git describe --tag --abbrev=0)
versionStatus=$?
if [[ $versionStatus -ne 0 ]]; then
    echo "No existe ningun TAG. Es necesario tener un TAG en Git para continuar con el despliegue"
    exit $versionStatus
fi

echo "Construyendo la version $version"
echo $version > version
git checkout $version
gradle clean build
echo "Construido"

echo "Empaquetando..."
tar -cvf deployment-package.tar build/libs/grandioso-nombre-del-proyecto-$version.jar etc/startup.sh etc/deploy.sh version
echo "Paquete de despliegue creado"

