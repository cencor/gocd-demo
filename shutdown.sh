#!/bin/bash
##
# Quote Service
# By: @gvargas
# Version: @version
# Name: shutdown.sh
# Description: tipo de cambio service stopper
##
echo "#########################################################################"
echo "#                Stopping tipo de cambio Service                        #"
echo "#########################################################################"
echo "-------------------------------------------------------------------------"

export LOG_FILE_PATH=
export OLDER_THAN=30
export TIPO_CAMBIO_SERVICE_PROCESS="TIPO_CAMBIO_SERVICE"
export LOG_DATE=`date "+%Y%m%d-%H%M%S"`

#
# <----------------------------- Don't edit after this line ------------------->
#
PID="$(ps -efa | grep java | grep $TIPO_CAMBIO_SERVICE_PROCESS | awk '{print $2}')"
if [ ! -z "${PID}" ]; then
	echo "Process is alive, sending SIGKILL..."
	kill -9 ${PID}
	echo "SIGKILL was sent"
	echo "-------------------------------------------------------------------------"
else
	echo "Process doesn't exist, verify if process is alive"
	echo "-------------------------------------------------------------------------"
	exit 1
fi

echo "Compressing logs..."
cd $LOG_FILE_PATH
tar -zcvf $LOG_FILE_PATH/tipo-cambio-service-$LOG_DATE.tar.gz *.log
rm $LOG_FILE_PATH/*.log
echo "-------------------------------------------------------------------------"
echo "On process to delete old files, $OLDER_THAN days before..."
find $LOG_FILE_PATH -mindepth 1 -mtime +$OLDER_THAN -delete
echo "Process finished with success"
echo "-------------------------------------------------------------------------"
exit 0
