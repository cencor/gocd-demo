#!/bin/sh
SUBJECT=`git log -1 --pretty=%s`
if [ `echo $SUBJECT | grep -c "Merge "` -ge 1 ]
    then
        # SSH Config para poder enviar cambios desde el gitlab runner
        mkdir -p ~/.ssh
        eval $(ssh-agent -s)
        ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
        echo "$AUTOVERSIONER_SSH_PRIVATE_KEY" | ssh-add -

        # Configura URL a traves de SSH para evitar enviar usuario y contrasenia por HTTP
        export SSH_REPOSITORY_URL="git@gitlab.com:"${CI_PROJECT_PATH}".git"
        git remote set-url --push origin "$SSH_REPOSITORY_URL"

        # Configura email y usuario para identificar los cambios hechos por el bot de CI
        git config --global user.email "ci@cencor.com"
        git config --global user.name "Continuous Integration Bot"

        # Autoversiona
        git checkout master
        git pull
        gradle clean release -Prelease.useAutomaticVersion=true
fi

